#!/bin/bash
# Este script organiza imagens em pastas por ano, mes e dia.
# move do diretorio de origem para destino de forma organizada.
# Feito por Wemerson Bruno Henriques 2020/07/09 - wbhenriques@gmail.com - v1

DIR_ORIGEM="$1"
DIR_DESTINO="$2"

if [ -z "$DIR_ORIGEM" ];then
        echo "Especifique a origem dos arquivos"
        exit 0
fi
if [ -z "$DIR_DESTINO" ];then
        echo "Especifique o destino dos arquivos"
        exit 0
fi
#sair () {
#       exit
#}

# Implementar um teste se o arquivo tmp não exite, em caso de interrupção abrupta

find "$DIR_ORIGEM" -type f >> /tmp/arquivos.tmp #&& cat /tmp/arquivos.tmp

while IFS= read -r linha || [[ -n "$linha" ]]; do
      DATA=$(stat --format=%y "$linha" | awk {'print $1'})
      ANO=$(echo $DATA|awk -F- {'print $1'})
      MES=$(echo $DATA|awk -F- {'print $2'})
      DIA=$(echo $DATA|awk -F- {'print $3'})
      mkdir -vp "$DIR_DESTINO/$ANO/$MES/$DIA"
      ##find "$linha" -mmin +30 -exec mv {} "$DIR_DESTINO/$ANO/$MES/$DIA" \;
      mv -u "$linha" "$DIR_DESTINO"/"$ANO"/"$MES"/"$DIA" #1> /dev/null
      #echo "$DATA"
done < /tmp/arquivos.tmp
  
#echo $ARQUIVO
#rm /tmp/arquivos.tmp

# Remove Arquivos que foram modificados a mais de 60 dias:
#find $DIR_DESTINO/ -mtime +60 -exec rm -rf {} \; && \

# Remove pastas vazias:
#find "$DIR_DESTINO" -depth -type d -empty -print -exec rmdir {} \;
find "$DIR_ORIGEM" -depth -type d -empty -print -exec rmdir {} \;

# Ao final do script toda pasta que permacer vazia será excluida,
# Caso algum arquivo ainda permaneça o mesmo poderá estar duplicado,
# sugiro que verifiquem com atenção estes que sobrarem na pasta.